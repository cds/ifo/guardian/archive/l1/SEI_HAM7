# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState
from guardian import NodeManager

from .. import const as top_const
from ..manager import *

##################################################

sub_names = dict(
    HPI = 'HPI_' + top_const.CHAMBER,
    ISI = 'ISI_' + top_const.CHAMBER,)
nodes = NodeManager(sub_names.values())

##################################################

class INIT(GuardState):
    request = True

    @watchdog_dackill_check
    def main(self):
        log("initializing subordinate nodes...")

        nodes.set_managed()

        self.wait_for_isi_masterswitch = False
        
        if watchdog_tripped_nodes(nodes):
            return 'WATCHDOG_TRIPPED_SUBORDINATE'

        if [node for node in nodes if node.state == 'INIT']:
            log("nodes in INIT; waiting for resolution...")
            self.return_state = 'INIT'

        elif not is_hpi_masterswitch_on():
            log('HPI masterswitch disengaged...')
            nodes[sub_names['HPI']] = 'READY'
            if not is_isi_masterswitch_on():
                nodes[sub_names['ISI']] = 'READY'
                self.return_state = 'OFFLINE'
            else: 
                nodes[sub_names['ISI']] = 'DAMPED'
                masterswitches_on(nodes)
                self.return_state = 'TURNING_ON_HPI_ISOLATION_LOOPS'
        
        elif not is_isi_masterswitch_on():
            log('ISI masterswitch disengaged...')
            self.wait_for_isi_masterswitch = True
            nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']
            nodes[sub_names['ISI']] = 'DAMPED'
            self.return_state = 'DAMPED'
            
        elif nodes[sub_names['HPI']] == ISOLATED_STATE['HPI']:
            log("HPI is ISOLATED...")
            nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']

            if nodes[sub_names['ISI']] == ISOLATED_STATE['ISI']:
                log("ISI is ISOLATED...")
                nodes[sub_names['ISI']] = ISOLATED_STATE['ISI']
                self.return_state = 'ISOLATED'

            else:
                log("ISI not ISOLATED...")
                log("resetting ISI to DAMPED...")
                nodes[sub_names['ISI']] = 'DAMPED'
                self.return_state = 'DAMPED'

        elif nodes[sub_names['HPI']] == 'READY':
            log("HPI is READY...")
            log("resetting everything to ready...")
            nodes[sub_names['HPI']] = 'READY'
            nodes[sub_names['ISI']] = 'READY'
            self.return_state = 'READY'

        else:
            log("unknown configuration (HPI = '%s'); resetting..." % nodes[sub_names['HPI']].state)
            nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']
            nodes[sub_names['ISI']] = 'DAMPED'
            self.return_state = 'DAMPED'

        log("next state: %s" % self.return_state)

    @get_subordinate_watchdog_check_decorator(nodes)
    @watchdog_dackill_check
    @nodes.checker()
    def run(self):
        if self.return_state == 'INIT':
            nodes_in_init = [node.name for node in nodes if node.state == 'INIT']
            if nodes_in_init:
                log("waiting for nodes to resolve INIT: %s" % nodes_in_init)
                return
            else:
                return 'INIT'

        if self.wait_for_isi_masterswitch:
            if not is_isi_masterswitch_on():
                notify("waiting for ISI masterswitch to be engaged...")
                return

        for stalled_node in nodes.get_stalled_nodes():
            log("%s is stalled (request=%s, state=%s)" % (stalled_node.name, stalled_node.request, stalled_node.state))
            stalled_node.revive()
            
        if nodes.arrived:
            return self.return_state


WATCHDOG_TRIPPED_SUBORDINATE = get_watchdog_tripped_subordinate_state(nodes)

OFFLINE = get_offline_state(nodes)
READY = get_ready_state(nodes)

TURNING_ON_ISI_DAMPING_LOOPS = get_move_control_loops_state(nodes, sub_names['ISI'], 'DAMPED')

ISI_DAMPED_HEPI_OFFLINE = get_idle_state(nodes, requestable=True)
ISI_DAMPED_HEPI_OFFLINE.index = 35

TURNING_ON_HPI_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['HPI'], ISOLATED_STATE['HPI'])
##########
# ISI_OFFLINE_HEPI_ON
TURN_ON_ISI_DAMPING_HEPI_UP = get_move_control_loops_state(nodes, sub_names['ISI'], 'DAMPED')

TURN_OFF_ISI_DAMPING_HEPI_UP = get_move_control_loops_state(nodes, sub_names['ISI'], 'READY')

ISI_OFFLINE_HEPI_ON = get_idle_state(nodes, requestable=True)
ISI_OFFLINE_HEPI_ON.index = 40
##########
DAMPED = get_idle_state(nodes, requestable=True)
DAMPED.index = 50

TURNING_ON_ISI_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['ISI'], ISOLATED_STATE['ISI'])

ISOLATED = get_idle_state(nodes, requestable=True)
ISOLATED.index = 200

TURNING_OFF_ISI_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['ISI'], 'DAMPED')

TURNING_OFF_HPI_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['HPI'], 'READY')
TURNING_OFF_ISI_DAMPING_LOOPS = get_move_control_loops_state(nodes, sub_names['ISI'], 'READY')
